component {
	function configure(){

		route( "cbadmin/module/oisThemeSettings/home/:moduleAction" )
			.rcAppend( { moduleHandler : "home" } )
			.to( "contentbox-admin:modules.execute" );

		route( "/:handler?/:action?" )
			.rcAppend( { handler : "home", action : "notFoundFacade" } )
			.to( "oisThemeSettings:home.notFoundFacade" );

	}
}
