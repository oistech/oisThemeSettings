if( !window.hasOwnProperty('oisThemeSettings') ){
    window.oisThemeSettings = {};
}

window.oisThemeSettings.jsonFromForm = function($form) {
	var $fieldset = $form.children("fieldset").first();
	var jsonObject = window.oisThemeSettings.dataFromFieldset($fieldset);

	return JSON.stringify(jsonObject, null, '\t')
	// return JSON.stringify(jsonObject);
}

window.oisThemeSettings.dataFromFieldset = function($fieldset) {
	var serializeAs = $fieldset.data("serialize-as");
	var jsonObject = null;

	switch (serializeAs) {
		case "object":
			jsonObject = {};
			var $children = $fieldset.children("fieldset");
			$children.each(function(index, fieldset) {
				var $fieldset = $(fieldset);
				var label = $fieldset.data("serialize-label");
				jsonObject[label] = window.oisThemeSettings.dataFromFieldset($fieldset);
			});

			break;
		case "array":
			jsonObject = [];
			var $children = $fieldset.children("fieldset");
			$children.each(function(index, fieldset) {
				var $fieldset = $(fieldset);
				var value = window.oisThemeSettings.dataFromFieldset($fieldset);
				if (value !== "" && value !== null)
					jsonObject.push(window.oisThemeSettings.dataFromFieldset($fieldset));
			});

			break;
		case "primitive":
			var $child = $fieldset.children("input").first();
			jsonObject = $child.val();
			if (jsonObject != "" && !isNaN(jsonObject))
				jsonObject = parseFloat(jsonObject);
			break;
	}
	return jsonObject;
}

window.oisThemeSettings.formFromJson = function(json) {
	var $form = $("<form>").addClass("form-horizontal");
	var parsed = $.parseJSON(json);
	if (Array.isArray(parsed)) {
		var $fields = window.oisThemeSettings.fieldsFromArray( parsed );
		$form.append($fields);
	} else {
		var $fields = window.oisThemeSettings.fieldsFromObject(parsed );
		$form.append($fields);
	}
	return $form;
}

window.oisThemeSettings.fieldsFromArray = function(arr, label) {
	var $fieldset = $("<fieldset/>")
		.addClass("array")
		.data("serialize-as", "array")
		.data("serialize-label", label);

	if (label) {
		var $legend = $("<legend>")
			.text(label)
			.appendTo($fieldset);
	}

	var replicate = null;

	// If this is an empty array, we need it to have SOME value
	if (arr.length == 0) {
		arr = [""];
	}

	for (var x in arr) {
		var item = arr[x];
		if ((!!item) && (item.constructor === Object)) {
			// This is an object
			var $fields = window.oisThemeSettings.fieldsFromObject( item, "JSON Object" );
			var $remove = window.oisThemeSettings.getDeleteButton($fields);
			replicate = $fields;
			$fields.append($remove);
			$fieldset.append($fields);
		} else if (Array.isArray(item)) {
			// This is an array
			var $fields = window.oisThemeSettings.fieldsFromArray( item, "JSON Array" );
			var $remove = window.oisThemeSettings.getDeleteButton($fields);
			replicate = $fields;
			$fields.append($remove);
			$fieldset.append($fields);
		} else {
			// This is a primitive
			var $fields = window.oisThemeSettings.fieldFromPrimitive( item, "JSON Primitive" );
			var $remove = window.oisThemeSettings.getDeleteButton($fields);
			replicate = $fields;
			$fields.append($remove);
			$fieldset.append($fields);
		}
	}
	var $replicate = window.oisThemeSettings.getReplicatorButton(replicate);
	$fieldset.append($replicate);
	return $fieldset;
}

window.oisThemeSettings.fieldsFromObject = function( obj, label ) {
	var $fieldset = $("<fieldset/>")
		.addClass("object")
		.data("serialize-as", "object")
		.data("serialize-label", label);

	var $legend = $("<div>")
		.addClass("object-label")
		.text(label)
		.appendTo($fieldset);

	for (var x in obj) {
		var item = obj[x];
		if ((!!item) && (item.constructor === Object)) {
			// This is an object
			var $fields = window.oisThemeSettings.fieldsFromObject(item);
			$fieldset.append($fields);
		} else if (Array.isArray(item)) {
			// This is an array
			var $fields = window.oisThemeSettings.fieldsFromArray(item, x);
			$fieldset.append($fields);
		} else {
			// This is a primitive
			var $field = window.oisThemeSettings.fieldFromPrimitive(item, x);
			$fieldset.append($field);
		}
	}

	return $fieldset;
}

window.oisThemeSettings.fieldFromPrimitive = function(value, label) {
	var $fieldset = $("<fieldset/>")
		.addClass("primitive")
		.data("serialize-as", "primitive")
		.data("serialize-label", label);

	if (label) {
		var $legend = $("<label>")
			.text(label + ":")
			.appendTo($fieldset);
	}

	var $input = $("<input>")
		.attr("type", "text")
        .addClass("form-control")
		.val(value)
		.appendTo($fieldset);

	return $fieldset;
}

window.oisThemeSettings.getReplicatorButton = function($fieldset) {
	var $button = $("<button>")
		.text("+")
		.addClass("btn btn-success replicate-button")
        .css( "border-radius" ,"0" )
		.click(function() {
			// Create the clone
			var $newFieldset = $fieldset.clone(true);

			// Clear the values
			$newFieldset.find("input")
				.val("");

			// Replace buttons
			var $replicatorButtons = $newFieldset.find(".replicate-button");
			$replicatorButtons.each(function(index, button) {
				var $button = $(button);
				$button.replaceWith(window.oisThemeSettings.getReplicatorButton($button.parent().children("fieldset").first()));
			});
			var $replicatorButtons = $newFieldset.find(".delete-button");
			$replicatorButtons.each(function(index, button) {
				var $button = $(button);
				$button.replaceWith(window.oisThemeSettings.getDeleteButton($button.parent()));
			});

			// Add the new fieldset
			$newFieldset.insertBefore($button);
			return false;
		});
	return $button;
}

window.oisThemeSettings.getDeleteButton = function($fieldset) {
	var $button = $("<button>")
		.text("x")
		.addClass("btn btn-danger btn-sm delete-button")
		.click(function() {
			$fieldset.remove();
			return false;
		})
	return $button;
}

window.oisThemeSettings.openJsonEditor = function( inputName ){
    var inputElement = $(`#${inputName}`);
    var inputJSON = inputElement.val().trim();
    if( inputJSON.length ){
        try {
            JSON.parse( inputJSON );
        }catch(e){
            adminNotifier( "error", "The value is not valid JSON", "6000" );
            return null;
        }
    }else{
        if( inputElement.data("oisJSONDefault") === undefined ){
            adminNotifier( "error", "The value is not valid JSON", "6000" );
            return null;
        }else{
            inputJSON = inputElement.data("oisJSONDefault").trim();
            try {
                JSON.parse( inputElement.data("oisJSONDefault").trim() );
            }catch(e){
                adminNotifier( "error", "The default value is not valid JSON", "6000" );
                return null;
            }
        }
    }
    var jsonForm= window.oisThemeSettings.formFromJson( inputJSON );
    var jsonModal = $('#oisThemeJsonSelectorModal');
    jsonModal.data( "inputName", inputName );
    jsonModal.find(".oisTheme-json-edit-form").empty().append( jsonForm );
    jsonModal.modal('show');
}

window.oisThemeSettings.editJsonCallback = function(){
    var jsonModal = $('#oisThemeJsonSelectorModal');
    var inputName = jsonModal.data( "inputName" );
    var jsonForm = jsonModal.find(".oisTheme-json-edit-form>form");
    var jsonString = window.oisThemeSettings.jsonFromForm( jsonForm );
    $(`#${inputName}`).val( jsonString );
    jsonModal.modal('hide');
}