component {

	property name="settingService"		inject="id:settingService@contentbox";
   	property name="cbMessagebox" 	    inject="messagebox@cbmessagebox";
	property name="themeService" 		inject="themeService@contentbox";
	property name="markdown" 			inject="Processor@cbmarkdown";
	property name="cbHelper" 			inject="CBHelper@cb";

	function preHandler( event, rc, prc, action, eventArguments ){
		// Only allow viewing from the /cbadmin/module/oisThemeSettings/:hander/:action route
		if( ucase( left( event.getContext().event , len("contentbox-admin") ) ) != "contentbox-admin" ){
			event.overrideEvent( 'oisThemeSettings:home.notFoundFacade' );
		}
	} // preHandler()

	function index(event,rc,prc){
		prc.saveURL	= prc.CBHelper.buildModuleLink("oisThemeSettings","home.saveSettings");
		prc.oisThemeSettings = prc.cbhelper.getmoduleService().getModuleConfigCache().oisThemeSettings;
		prc.themeService = themeService;

		// Load default module settings
		var moduleSettings = prc.CBHelper.getModuleSettings("oisThemeSettings");
		for ( var currentKey in moduleSettings) { 
			event.paramValue( currentKey, moduleSettings[currentKey] );
		} 
		// set existing setting values
		var allsettings = settingService.findWhere( criteria = { name : "oisThemeSettings" } );
		if( !isNull( allsettings ) ){
			var pairs = deserializeJSON( allsettings.getValue() );
			for( var key in pairs ){
				event.setValue( key, pairs[key] );
			}
		}
		prc.faVersions = event.getValue('faVersions');
		event.setView( view= "home/index" );
	} // index()

	function saveSettings( event, rc, prc ){
		// read the module settings
		var moduleSettings = prc.CBHelper.getModuleSettings("oisThemeSettings");
		// get the ContentBox Settings from settingService
        var oSettings = settingService.findWhere( criteria = { name : "oisThemeSettings" } );
		if( isNull( oSettings ) ){
			oSettings = settingService.new( properties = { name : "oisThemeSettings" } );
		}
		// Get modules settings from ContentBox settingService
        var cbModuleSettings = !isNull( oSettings ) ? deserializeJson( oSettings.getValue() ) : {};
		// Set the keys I want to update
        var settingsKeyToUpdate = [ "enableThemeSettings", "themeKeysImage", "themeKeysLinks", "themeKeysJSON", "themeKeysIcons", "faIncludeIconTag", "faToLoad" ];
        // update moduleSettings struct from current ContentBox settings service data
		for ( var currentKey in settingsKeyToUpdate ){ 
            if( rc.keyExists( currentKey ) ){
                moduleSettings[ currentKey ] = rc[ currentKey ];
            }else if( cbModuleSettings.keyExists( currentKey ) ){
				moduleSettings[ currentKey ] = cbModuleSettings[ currentKey ];
			}
		}
		// Save the settings
		oSettings.setValue( serializeJSON( moduleSettings ) )
		settingService.save( oSettings );
		settingService.flushSettingsCache();
		// Set Messagebox
		cbMessageBox.info("Settings Saved & Updated!");
		// Relocate with CB Helper
		prc.CBHelper.setNextModuleEvent( "oisThemeSettings", "home.index" );
	} // saveSettings()

	function iconSelector( event, rc, prc ){
		// Load default module settings
		var moduleSettings = prc.CBHelper.getModuleSettings("oisThemeSettings");
		for ( var currentKey in moduleSettings) { 
			event.paramValue( currentKey, moduleSettings[currentKey] );
		} 
		// set existing setting values
		var allsettings = settingService.findWhere( criteria = { name : "oisThemeSettings" } );
		if( !isNull( allsettings ) ){
			var pairs = deserializeJSON( allsettings.getValue() );
			for( var key in pairs ){
				event.setValue( key, pairs[key] );
			}
		}
		event.setView( view= "home/iconSelector" ).noLayout();
	} // iconSelector()

	function about( event, rc, prc ){
		// Load readme.md and convert to html then output on about page
		var readme_path = prc.cbhelper.getmoduleService().getmoduleRegistry().oisThemeSettings.PHYSICALPATH & "/oisThemeSettings/README.md";
		var readme_contents = fileRead( readme_path );
		prc.readme_html = variables.markdown.toHTML( readme_contents );
		event.setView( view= "home/about" );
	} // about()

	/*
		Uses the standard ContentBox page layout and notFound view to mimic the "oopsy" page (404)
		Called From: 
			home.preHandler: 
				request is not comming from /cbadmin/module/oisThemeSettings/ (withing the ContentBox Admin)
			home.onMissingAction:
				handler was correct (home) but action wasn't found
			themeSettingsInterceptor.onInvalidEvent:
				catches module invalid events ( I.E. /oisThemeSettings/handerNameThatDoesNotExists )
	*/
	function notFoundFacade( event, rc, prc ){
		cbHelper.prepareUIRequest();
		event.setPrivateValue( "missingPage", prc.currentRoutedURL );
		event
			.setLayout( name = "#themeService.getActiveTheme().name#/layouts/pages", module = themeService.getActiveTheme().module )
			.setView( view = "#themeService.getActiveTheme().name#/views/notFound", module = themeService.getActiveTheme().module  );
	} // notFoundFacade()

	function onMissingAction( event, rc, prc, missingAction, eventArguments ){
		event.overrideEvent( 'oisThemeSettings:home.notFoundFacade' );
	} // onMissingAction()

}