<cfoutput>

    <!--- INIT window.oisThemeSettings --->
    <script>
        if( !window.hasOwnProperty('oisThemeSettings') ){
            window.oisThemeSettings = {};
        }
    </script>   
    <!--- / INIT window.oisThemeSettings --->

    <cfif oisThemeExtendedImageFields.len() >
        <!--- PROCESS IMAGE SELECTORS --->
        <script>
            window.oisThemeSettings.oisCBThemeopenImageBrowser = function( oisSettingsInputName ){
                openRemoteModal(
                    "/#prc.CBADMINENTRYPOINT#/ckFileBrowser/assetChooser?callback=window.oisThemeSettings.oisCBThemeImageSelectCallback&filterType=Image",
                    { "oisSettingsInputName" : oisSettingsInputName },
                    "75%",
                    "75%"
                );
            }
            window.oisThemeSettings.oisCBThemeImageSelectCallback = function( filePath, fileURL, fileType ){
                var inputName = $remoteModal.data("params").oisSettingsInputName;
                if( fileURL.length > 0 ){
                    $("##" + inputName.toString() ).val( fileURL );
                }
                closeRemoteModal();
            }
            window.oisThemeSettings.oisImageButtons = {};
            document.addEventListener( "DOMContentLoaded", () => {
                // <cfloop index="currentIndex" item="currentItem" array="#oisThemeExtendedImageFields#">
                    $("##cb_theme_#prc.activeTheme.name#_#currentItem.name#").css({ "margin-bottom" : "5px" });
                    window.oisThemeSettings.oisImageButtons[ "#currentItem.name#" ] = $(`<button type="button" class="btn btn-info btn-sm" id="cb_theme_#prc.activeTheme.name#_#currentItem.name#_oisimagebutton" onclick="window.oisThemeSettings.oisCBThemeopenImageBrowser('cb_theme_#prc.activeTheme.name#_#currentItem.name#')"></button>`);
                    window.oisThemeSettings.oisImageButtons[ "#currentItem.name#" ].html('#currentItem.oisButtonIcon# #currentItem.oisButtonLabel#');
                    window.oisThemeSettings.oisImageButtons[ "#currentItem.name#" ].insertAfter( $("##cb_theme_#prc.activeTheme.name#_#currentItem.name#") );
                // </cfloop>
            });
        </script>
        <!--- / PROCESS IMAGE SELECTORS --->
    </cfif>
    
    <cfif oisThemeExtendedLinkFields.len() >
        <!--- PROCESS LINK SELECTORS --->
        <script>
            window.oisThemeSettings.oisCBThemeopenLinkBrowser = function( oisSettingsInputName ){
                openRemoteModal(
                    "/#prc.CBADMINENTRYPOINT#/content/showRelatedContentSelector?contentType=Page,Entry",
                    { "oisSettingsInputName" : oisSettingsInputName },
                    "75%",
                    "75%"
                );
            }
            window.oisThemeSettings.oisCBThemeopenLinkBrowserCallback = function( contentID, contentTitle, contentType, contentSlug ){
                event.preventDefault();
                var inputName = $remoteModal.data("params").oisSettingsInputName;
                var linkURL = contentSlug;
                if( contentType.toLowerCase() == "page" ){
                    linkURL = "/" + linkURL;
                }else{
                    linkURL = `/#prc.cbHelper.getBlogEntryPoint()#/${linkURL}`;
                }
                $("##" + inputName.toString() ).val( linkURL );
                closeRemoteModal();
            }
            // redirect expected contentbox function to window.oisThemeSettings.oisCBThemeopenLinkBrowserCallback
            function chooseRelatedContent( contentID, contentTitle, contentType, contentSlug ){
                return window.oisThemeSettings.oisCBThemeopenLinkBrowserCallback( contentID, contentTitle, contentType, contentSlug );
            }
            window.oisThemeSettings.oisLinkButtons = {};
            document.addEventListener( "DOMContentLoaded", () => {
                // <cfloop index="currentIndex" item="currentItem" array="#oisThemeExtendedLinkFields#">
                    $("##cb_theme_#prc.activeTheme.name#_#currentItem.name#").css({ "margin-bottom" : "5px" });
                    window.oisThemeSettings.oisLinkButtons[ "#currentItem.name#" ] = $(`<button type="button" class="btn btn-info btn-sm" id="cb_theme_#prc.activeTheme.name#_#currentItem.name#_oisimagebutton" onclick="window.oisThemeSettings.oisCBThemeopenLinkBrowser('cb_theme_#prc.activeTheme.name#_#currentItem.name#')"></button>`);
                    window.oisThemeSettings.oisLinkButtons[ "#currentItem.name#" ].html('#currentItem.oisButtonIcon# #currentItem.oisButtonLabel#');
                    window.oisThemeSettings.oisLinkButtons[ "#currentItem.name#" ].insertAfter( $("##cb_theme_#prc.activeTheme.name#_#currentItem.name#") );
                // </cfloop>
            });
        </script>
        <!--- / PROCESS LINK SELECTORS --->
    </cfif>

    <cfif oisThemeExtendedIconFields.len() >
        <!--- PROCESS FONT-AWESOME SELECTORS --->
        <div class="modal fade" id="oisThemeIconSelectorModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4>#getProperty("faSelectorTitle")#</h4>
                    </div>
                    <div class="modal-body iconBoxWrapper" id="oisThemeIconSelectorModalBody">
                        <iframe id="oisIconSelectorIFrame" src="" width="100%" height="500"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            window.oisThemeSettings.faIconSelectorsInit = function(){
                $('##oisThemeIconSelectorModal').modal({ keyboard: false, show: false });
                $('##oisThemeIconSelectorModal').on('hidden.bs.modal', function () {
                    $("##oisIconSelectorIFrame").attr("src", "");
                });
                window.oisThemeSettings.oisIconButtons = {};
                // <cfloop index="currentIconIndex" item="currentIcon" array="#oisThemeExtendedIconFields#">
                    $("##cb_theme_#prc.activeTheme.name#_#currentIcon.name#").css({ "margin-bottom" : "5px" });
                    window.oisThemeSettings.oisIconButtons[ "#currentIcon.name#" ] = $(`<button type="button" class="btn btn-info btn-sm" id="cb_theme_#prc.activeTheme.name#_#currentIcon.name#_oisiconbutton" onclick="window.oisThemeSettings.openIconSelector('cb_theme_#prc.activeTheme.name#_#currentIcon.name#')"></button>`);
                    window.oisThemeSettings.oisIconButtons[ "#currentIcon.name#" ].html('#currentIcon.oisButtonIcon# #currentIcon.oisButtonLabel#');
                    window.oisThemeSettings.oisIconButtons[ "#currentIcon.name#" ].insertAfter( $("##cb_theme_#prc.activeTheme.name#_#currentIcon.name#") );
                // </cfloop>
            }
            window.oisThemeSettings.openIconSelector = function( inputName ){
                $("##oisIconSelectorIFrame").attr("src", `#cbHelper.buildModuleLink('oisThemeSettings','home.iconSelector')#?setting=${inputName}`);
                $('##oisThemeIconSelectorModal').modal('show');
            }
            window.oisThemeSettings.selectIconCallback = function( inputName, iconClass ){
                $(`##${inputName}`).val( #getProperty("faIncludeIconTag", false) ? "`<i class='${iconClass}'></i>`" : "iconClass"# );
                $('##oisThemeIconSelectorModal').modal('hide');
                $("##oisIconSelectorIFrame").attr("src", "");
            }
            document.addEventListener( "DOMContentLoaded", () => {
                window.oisThemeSettings.faIconSelectorsInit();
            });
        </script>
        <!--- / PROCESS FONT-AWESOME SELECTORS --->
    </cfif>

    <cfif oisThemeExtendedJsonFields.len() >
        <!--- PROCESS JSON SELECTORS --->
        <cfscript>
            cfhtmlhead( text='
                <link href="#prc.cbhelper.getmoduleService().getmoduleRegistry().oisThemeSettings.LOCATIONPATH#/oisThemeSettings/includes/css/oisTheme.css" rel="stylesheet" type="text/css">
                <script type="text/javascript" src="#prc.cbhelper.getmoduleService().getmoduleRegistry().oisThemeSettings.LOCATIONPATH#/oisThemeSettings/includes/js/oisTheme.js"></script>'
            );
        </cfscript>
        <div class="modal fade" id="oisThemeJsonSelectorModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding: 0 15px;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3>#getProperty("jsonEditorTitle")#</h3>
                    </div>
                    <div class="modal-body" style="padding: 0;" id="oisThemeJsonSelectorModalBody">
                        <div class="oisTheme-json-edit-form"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success" onclick="window.oisThemeSettings.editJsonCallback();">Finished</button>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            window.oisThemeSettings.faJsonSelectorsInit = function(){
                $('##oisThemeJsonSelectorModal').modal({ keyboard: false, show: false });
                $('##oisThemeJsonSelectorModal').on('hidden.bs.modal', function () {
                    $('##oisThemeJsonSelectorModal').removeData( "inputName" );
                    $('##oisThemeJsonSelectorModal').find(".oisTheme-json-edit-form").empty();
                });
                window.oisThemeSettings.oisJsonButtons = {};
                // <cfloop index="currentJsonIndex" item="currentJson" array="#oisThemeExtendedJsonFields#">
                    $("##cb_theme_#prc.activeTheme.name#_#currentJson.name#").css({ "margin-bottom" : "5px" });
                    window.oisThemeSettings.oisJsonButtons[ "#currentJson.name#" ] = $(`<button type="button" class="btn btn-info btn-sm" id="cb_theme_#prc.activeTheme.name#_#currentJson.name#_oisiconbutton" onclick="window.oisThemeSettings.openJsonEditor('cb_theme_#prc.activeTheme.name#_#currentJson.name#')"></button>`);
                    window.oisThemeSettings.oisJsonButtons[ "#currentJson.name#" ].html('#currentJson.oisButtonIcon# #currentJson.oisButtonLabel#');
                    window.oisThemeSettings.oisJsonButtons[ "#currentJson.name#" ].insertAfter( $("##cb_theme_#prc.activeTheme.name#_#currentJson.name#") );
                    // <cfif currentJson.keyExists("oisJSONDefault") >
                        // <cfif isJson( currentJson.oisJSONDefault ) >
                            $("##cb_theme_#prc.activeTheme.name#_#currentJson.name#").data( "oisJSONDefault", "#encodeForJavaScript( currentJson.oisJSONDefault )#" );
                        // </cfif>
                    // </cfif>
                // </cfloop>
            }
            document.addEventListener( "DOMContentLoaded", () => {
                window.oisThemeSettings.faJsonSelectorsInit();
            });
        </script>
        <!--- / PROCESS JSON SELECTORS --->
    </cfif>

</cfoutput>