component extends="coldbox.system.Interceptor" {

	property name="settingService"  inject="settingService@contentbox";
	property name="CBHelper"        inject="id:CBHelper@contentbox";

    function configure(){
        updateSettings();
    } // configure()

    /* INSERT BADGE AT TOP OF SETTINGS FORM BETWEEN "THEME MODULES" AND "THEME SETTINGS" TITLE */
    public function cbadmin_onThemeInfo( event, interceptData, buffer, rc, prc ){
        // update settings
        updateSettings();
        var isEnabled = getProperty( "enableThemeSettings");
        if( isNull( isEnabled ) ){
            return javaCast("null","");
        }else if( !isEnabled ){
            return javaCast("null","");
        }
        var oisCBModuleSettings = prc.cbhelper.getmoduleService().getModuleConfigCache().oisThemeSettings;
        arguments.buffer.append( '
        <div style="margin-top: 15px;">
            <span class="label label-info" style="padding: 5px; border-radius: 0.25em;">
                #oisCBModuleSettings.title# (#oisCBModuleSettings.version#) Provided By 
                <a href="#oisCBModuleSettings.weburl#" target="_blank" style="color: white !important;">OIS Technologies</a>
            </span>
        </div>' );
    } // cbadmin_onThemeInfo()

    /* AFTER ACTIVE THEME FORM INPUTS AND ABOVE SAVE BUTTON */
    public function cbadmin_onThemeSettings( event, interceptData, buffer, rc, prc ){
        // update settings
        updateSettings();
        var isEnabled = getProperty( "enableThemeSettings");
        if( isNull( isEnabled ) ){
            return javaCast("null","");
        }else if( !isEnabled ){
            return javaCast("null","");
        }
        prc.oisCBModuleSettings = prc.cbhelper.getmoduleService().getModuleConfigCache().oisThemeSettings;
        /* IMAGE SETTINGS KEYS */
        var imageSettingsKeys = getProperty("themeKeysImage");
        var oisThemeExtendedImageFields = [];
        for( var inputName IN listToArray( imageSettingsKeys ) ){
            oisThemeExtendedImageFields.append( normalizeSettingData( inputName, getProperty("buttonDefaults").images.label, getProperty("buttonDefaults").images.icon ) );
        }
        /* FONT-AWESOME SETTINGS KEYS */
        var iconSettingsKeys = getProperty("themeKeysIcons");
        var oisThemeExtendedIconFields = [];
        for( var inputName IN listToArray( iconSettingsKeys ) ){
            oisThemeExtendedIconFields.append( normalizeSettingData( inputName, getProperty("buttonDefaults").icons.label, getProperty("buttonDefaults").icons.icon ) );
        }
        /* LINK SETTINGS KEYS */
        var linkSettingsKeys = getProperty("themeKeysLinks");
        var oisThemeExtendedLinkFields = [];
        for( var inputName IN listToArray( linkSettingsKeys ) ){
            oisThemeExtendedLinkFields.append( normalizeSettingData( inputName, getProperty("buttonDefaults").links.label, getProperty("buttonDefaults").links.icon ) );
        }
        /* JSON SETTINGS KEYS */
        var jsonSettingsKeys = getProperty("themeKeysJSON");
        var oisThemeExtendedJsonFields = [];
        for( var inputName IN listToArray( jsonSettingsKeys ) ){
            oisThemeExtendedJsonFields.append( normalizeSettingData( inputName, getProperty("buttonDefaults").json.label, getProperty("buttonDefaults").json.icon ) );
        }
        /* LOOP OVER SETTINGS FROM THEME AND LOOK FOR oisImageSelect, oisIconSelect, oisLinkSelect, oisJsonSelect */
        for( var setting IN  prc.activeTheme.settings ){
            /* IMAGE SETTINGS KEYS */
            if( setting.keyExists("oisImageSelect") ){
                if( !oisThemeExtendedImageFields.find( function( item ){ return lcase(item.name) == lcase( setting.name );} ) ){
                    oisThemeExtendedImageFields.append( normalizeSettingData( setting, getProperty("buttonDefaults").images.label, getProperty("buttonDefaults").images.icon ) );
                }
            }
            /* FONT-AWESOME SETTINGS KEYS */
            if( setting.keyExists("oisIconSelect") ){
                if( !oisThemeExtendedIconFields.find( function( item ){ return lcase(item.name) == lcase( setting.name );} ) ){
                    oisThemeExtendedIconFields.append( normalizeSettingData( setting, getProperty("buttonDefaults").icons.label, getProperty("buttonDefaults").icons.icon ) );
                }
            }
            /* LINK SETTINGS KEYS */
            if( setting.keyExists("oisLinkSelect") ){
                if( !oisThemeExtendedLinkFields.find( function( item ){ return lcase(item.name) == lcase( setting.name );} ) ){
                    oisThemeExtendedLinkFields.append( normalizeSettingData( setting, getProperty("buttonDefaults").links.label, getProperty("buttonDefaults").links.icon ) );
                }
            }
            /* JSON SETTINGS KEYS */
            if( setting.keyExists("oisJSONSelect") ){
                if( !oisThemeExtendedJsonFields.find( function( item ){ return lcase(item.name) == lcase( setting.name );} ) ){
                    oisThemeExtendedJsonFields.append( normalizeSettingData( setting, getProperty("buttonDefaults").json.label, getProperty("buttonDefaults").json.icon ) );
                }
            }
        } // setting IN  prc.activeTheme.settings
        /* USE INCLUDE FILE FOR OUTPUT */
        savecontent variable="oisThemeSettings_Script" {
            include "inc/interceptorOutput_ThemeSettingsForm.cfm";
        };
        arguments.buffer.append( oisThemeSettings_Script );
	} // cbadmin_onThemeSettings

    private function normalizeSettingData( inData, label, defaultIcon ){
        if( isSimpleValue( inData ) ){
            inData = { "name" : inData };
        }
        if( !inData.keyExists("oisButtonLabel") ){
            inData["oisButtonLabel"] = label;
        }
        if( !inData.keyExists("oisButtonIcon") ){
            inData["oisButtonIcon"] = defaultIcon;
        }
        return inData;
    } // normalizeSettingData()

    private function updateSettings(){
		// Load default module settings
		var moduleSettings = CBHelper.getModuleSettings("oisThemeSettings");
        var oSettings = settingService.findWhere( criteria = { name : "oisThemeSettings" } );
		if( isNull( oSettings ) ){
			oSettings = settingService.new( properties = { name : "oisThemeSettings" } );
		}
        if( isJson( oSettings.getValue() ) ){
            var cbModuleSettings = deserializeJson( oSettings.getValue() );
        }else{
            var cbModuleSettings = {};
        }
        var settingsKeyToUpdate = [ "enableThemeSettings", "themeKeysImage", "themeKeysLinks", "themeKeysIcons", "faIncludeIconTag", "faToLoad" ];
        // update moduleSettings struct from ContentBox settings service data
		for ( var currentKey in settingsKeyToUpdate ){ 
            if( cbModuleSettings.keyExists( currentKey ) ){
                moduleSettings[ currentKey ] = cbModuleSettings[ currentKey ];
            }
		} 
        // update propery values
        for ( var currentKey in moduleSettings ) { 
            setProperty( currentKey, moduleSettings[ currentKey ] );
        }
    } // updateSettings()

    /* 
        onInvalidEvent: checks to see if the interceptData.invalidEvent was pointing to this module and if so, overides. 
        Used to catch any root url uncaught errors, such as /oisThemeSettings/handerNameThatDoesNotExists
        if root url is /oisThemeSettings/home/actionThatDoesNotExist it is caught by the onMissingAction() in home.cfc handler
        acutal hander.event combinations from root are caught by Router.cfc and routed to home.notFoundFacade
    */
    public function onInvalidEvent( event, interceptData ){
        if( interceptData.keyExists("invalidEvent") ){
            if( left( interceptData.invalidEvent, len("#cbhelper.getmoduleService().getModuleConfigCache().oisThemeSettings.modelNamespace#:") ) == "#cbhelper.getmoduleService().getModuleConfigCache().oisThemeSettings.modelNamespace#:" ){
                interceptData.ehBean.setModule( cbhelper.getmoduleService().getModuleConfigCache().oisThemeSettings.modelNamespace );
                interceptData.ehBean.setHandler("home");
                interceptData.ehBean.setMethod("notFoundFacade");
                interceptData.override = true;
            }
        }
    }

}  