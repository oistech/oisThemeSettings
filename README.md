# OIS Theme Settings Extender

**Please Note**

The module has had fairly limited testing in environments other than ColdFusion 2018 on Windows with ContentBox 5.3 on ColdBox 6.8. Any feedback is welcome!

The OIS Theme Settings Extender adds extended functionality to the active theme’s settings page (ContentBox admin menu: Look & Feel > Active Theme). It does not modify the theme form input; it adds a button directly below that opens the configured extended functionality modal window and when completed updates the value of the settings input. Options can be configured in two ways. One, you can enter the setting name value from the theme settings configuration or two, the theme developer can add keys to each setting structure in the theme settings (theme.cfc). <a href="#how-do-i-get-these-buttons-to-show-up">More detailed information is provided below on these methods.</a> The OIS Theme Settings Extender provides four options for extended functionality.

- <a href="#image-selector">Image Selector</a>
- <a href="#link-selector">Link Selector</a>
- <a href="#json-editor">JSON Editor</a>
- <a href="#font-awesome-icon-selector">Font-Awesome Icon Selector</a>

When the OIS Theme Settings Extender is enabled it will display a small banner at the top of the active theme's settings page

![Settings Form Banner Screenshot](https://assets.oistech.com/packages/oiscbthemesettings/screen-shots/settings_banner.png)

When activated in ContentBox the OIS Theme Settings Extender will add a new ContentBox admin menu for OIS Modules with a sub-menu for Settings Extender

![ContentBox Admin Menu Screenshot](https://assets.oistech.com/packages/oiscbthemesettings/screen-shots/cbadmin-menu.png)

## Image Selector

Opens the standard ContentBox file browser allowing all the functions available in the file browser such as uploads, editing, renaming, etc. When a file is selected the settings input is updates with relative URL for the selected file.

**Button**

![Image Button Screenshot](https://assets.oistech.com/packages/oiscbthemesettings/screen-shots/image_button.png)

**Image Browser**

![Image Browser Screenshot](https://assets.oistech.com/packages/oiscbthemesettings/screen-shots/image_browser.png)
## Link Selector

Opens the standard ContentBox content browser to allow the selection of Pages and Blog Entries. When a Page or Blog Entry is selected the settings input is updates with relative URL for the selected content.

**Button**

![Link Button Screenshot](https://assets.oistech.com/packages/oiscbthemesettings/screen-shots/link_button.png)

**Link Browser**

![Link Browser Screenshot](https://assets.oistech.com/packages/oiscbthemesettings/screen-shots/link_browser.png)

## JSON Editor

Opens a basic JSON editor using the existing content of the input field. If the settings input does not contain JSON it looks for a default JSON template in the theme settings (See extended info on theme settings keys below). Both existing JSON and any default JSON template are validated using javascripts JSON.parse() function. If JSON.parse() fails an error will be displayed and the editor will not open. New array elements can be appended by using the big green "+" button at the bottom of any array. Note that if you have a mixed array ( strings, objects, and arrays ) the JSON editor always takes the last element of the array and appends an empty clone.

**Button**

![JSON Editor Button Screenshot](https://assets.oistech.com/packages/oiscbthemesettings/screen-shots/json_button.png)

**JSON Editor**

![JSON Editor Screenshot](https://assets.oistech.com/packages/oiscbthemesettings/screen-shots/json_editor.png)

## Font-Awesome Icon Selector

Opens a Font-Awesome icon selector using the configured version of Font-Awesome. It offers the option to either insert just the CSS classes or the full icon html tag. The module supports three versions of Font-Awesome. The versions supported as well as the corresponding links to the CDN used and the FontAwesome.com are provided on the OIS Theme Settings Extender settings page.

**Button**

![Font-Awesome Button Screenshot](https://assets.oistech.com/packages/oiscbthemesettings/screen-shots/icon_button.png)

**Font-Awesome Browser**

![Font-Awesome Browser Screenshot](https://assets.oistech.com/packages/oiscbthemesettings/screen-shots/icon_browser.png)


# How do I get these buttons to show up?

There are two ways to configure the module and they can both be used at the same time. Method one, the simplified method that uses all the defaults for button icons and labels and method two, add the proper key/value pairs to your theme settings in your theme.cfc

## Method One: OIS Theme Settings Extender Configuration Page

At the top of the module settings page will be a full list of all the active themes keys with a description and the key name to the right. When you click on a key name it is copied to your clipboard and you can paste that key into the coresponding inputs below. Keep mind this must be a comma seperated list of key names. Note in the list of theme keys if a key has a definiation as outlined in method two below it will in the list as a blue row with a tooltip informing you of the type of definition (Image, Link, JSON, or Icon)

![Module Settings](https://assets.oistech.com/packages/oiscbthemesettings/screen-shots/module_config.png)




## Method Two: Theme Configuration Settings Keys

Each option has corresponding keys to configure OIS Theme Settings Extender settings automatically

- **Image Selector**
    - oisImageSelect   {boolean} : Enables the image selector
    - oisButtonLabel   {string}  : Label for the button
    - oisButtonIcon    {string}  : Font-Awesome 5 icon to use for button (entire i tag)
- **Link Selector**
    - oisLinkSelect    {boolean} : Enables the link selector
    - oisButtonLabel   {string}  : Label for the button
    - oisButtonIcon    {string}  : Font-Awesome 5 icon to use for button (entire i tag)
- **JSON Editor**
    - oisJSONSelect    {boolean} : Enables the JSON editor
    - oisButtonLabel   {string}  : Label for the button
    - oisButtonIcon    {string}  : Font-Awesome 5 icon to use for button (entire i tag)
    - oisJSONDefault   {string}  : JSON string of a default array or structure to be used if settings input is blank or not valid JSON
- **Font-Awesome Icon Selector**
    - oisIconSelect    {boolean} : Enables the Font-Awesome selector
    - oisButtonLabel   {string}  : Label for the button
    - oisButtonIcon    {string}  : Font-Awesome 5 icon to use for button (entire i tag)

**Example Theme settings from theme.cfc**

```
this.settings = [
    {
        name                : "hpHeroSlides",
        group               : "Homepage | Hero Slideshow",
        defaultValue        : "",
        type                : "textarea",
        label               : "Homepage Hero Slideshow JSON:",
        fieldDescription    : "JSON Array of Homepage hero slides.<br>Background Images are best at 1920px X 1080px",
        oisJSONSelect       : true,
        oisButtonLabel      : "Edit JSON",
        oisButtonIcon       : "<i class='fab fa-js'></i>",
        oisJSONDefault      : '[{
                "title"         : "Hero One",
                "description"   : "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                "textcolor"     : "white",
                "buttontext"    : "Read More",
                "buttonurl"     : "/"
        }]'
    }
];
```