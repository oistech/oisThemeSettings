component {

	this.title 					= "OIS Theme Settings";
	this.author 				= "Michael Rigsby <mrigsby@oistech.com>";
	this.webURL 				= "http://www.oistech.com";
	this.description 			= "OIS Theme Settings extends the active theme settings form allowing the addition of Image Browsing, Link Browsing, Font Awesome Icon Selectors, and a basic JSON editor to the theme settings form (ContentBox admin menu: Look & Feel > Active Theme)";
	this.version				= "1.0.0";
	this.viewParentLookup 		= true;
	this.layoutParentLookup 	= true;
	this.entryPoint				= "oisThemeSettings";
	this.inheritEntryPoint 		= false;
	this.modelNamespace			= "oisThemeSettings";
	this.cfmapping				= "oisThemeSettings";
	this.autoMapModels			= true;
	this.dependencies       	= [ "contentbox-admin" ];

	// admin menu settings used below
	this.OISADMIN_TOPMENU		= "oisadminmenu";
	this.OISADMIN_TOPMENU_LABEL = "<i class='fas fa-laptop-code'></i> OIS Modules";

	this.OISADMIN_SUBMENU		= "oisThemeSettings";
	this.OISADMIN_SUBMENU_LABEL	= '<i class="fas fa-cogs" style="margin-right: 0;"></i> Settings Extender';

	function configure(){
		parentSettings = {
		};
		settings = {
			// EDITIABLE MODULE SETTINGS (INCLUDED IN SETTINGS PAGE)
			enableThemeSettings	:   false,
			themeKeysImage		:	"",
			themeKeysLinks		:	"",
			themeKeysJSON		:	"",
			themeKeysIcons		:	"",
			faIncludeIconTag	:   false,
			faToLoad			:	"6.4.0",
			// STATIC MODULE SETTINGS (NOT INCLUDED IN SETTINGS PAGE)
			buttonDefaults		: {
				"images"    : { "label" : "Select Image",   "icon" : '<i class="fas fa-images"></i>' },
				"icons"     : { "label" : "Select Icon",    "icon" : '<i class="fas fa-icons"></i>' },
				"links"     : { "label" : "Select Content", "icon" : '<i class="fas fa-sitemap"></i>' },
				"json"      : { "label" : "Edit JSON", 		"icon" : '<i class="fab fa-js"></i>' }
			},
			jsonEditorTitle		:	'<i class="fab fa-js"></i> Edit JSON',
			faSelectorTitle		:	'<i class="fas fa-icons"></i> Font-Awesome Icon Selector',
			faVersions			:	{
				"6.4.0" : {
					"url" 		: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css",
					"browse"	: "https://fontawesome.com/v6/search?o=r&m=free"
				},
				"5.15.4" : {
					"url"		: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css",
					"browse"	: "https://fontawesome.com/v5/search?o=r&m=free"
				},
				"4.7.0" : {
					"url"		: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css",
					"browse"	: "https://fontawesome.com/v4/icons/"
				} 
			},
			faThemes			:	{
				"regular"   : { "tabTitle" : "Regular" },
				"solid"     : { "tabTitle" : "Solid" },
				"brands"    : { "tabTitle" : "Brands" },
				"duotone"   : { "tabTitle" : "Duotone" }
			}
		};
		interceptorSettings = {
			customInterceptionPoints = []
		};
	} // configure()

	function onLoad(){
		manageAdminMenu("add");
		controller.getInterceptorService().registerInterceptor("#moduleMapping#.interceptors.themesettingsInterceptor");
	} // onLoad()

	function onActivate(){
		// store default settings
		var settingService = controller.getWireBox().getInstance("SettingService@contentbox");
		var setting = settingService.findWhere( criteria={ name="#this.modelNamespace#" } );
		if( isNull( setting ) ){
			var oisThemeSettingsSettings = settingService.new( properties={ name="#this.modelNamespace#", value=serializeJSON( settings ) } );
			settingService.save( oisThemeSettingsSettings );
		}
	} // onActivate()

	function onUnload(){
		manageAdminMenu("remove");
		controller.getInterceptorService().unregister( interceptorName="themesettingsInterceptor" );
	} // onUnload()

	function onDeactivate(){
		var settingService = controller.getWireBox().getInstance("SettingService@contentbox");
		var setting = settingService.findWhere( criteria={ name="#this.modelNamespace#" } );
		if( !isNull( setting ) ){
			settingService.delete( setting );
		}
	} // onDeactivate()

	function manageAdminMenu( action = "add" ){
		var menuService = controller.getWireBox().getInstance( "AdminMenuService@contentbox" );
		var topMenuMap = menuService.getPropertyMixin( "topMenuMap" );
		
		if( action == "add" ){
		
			if( !topMenuMap.keyExists( this.OISADMIN_TOPMENU ) ){
				menuService.addTopMenu(
					name	= this.OISADMIN_TOPMENU,
					label	= this.OISADMIN_TOPMENU_LABEL,
					href	= "##"
				);
			}

			menuService.addSubMenu( 
				topMenu	= this.OISADMIN_TOPMENU, 
				name	= this.OISADMIN_SUBMENU, 
				label	= this.OISADMIN_SUBMENU_LABEL,
				href	= "#menuService.buildModuleLink( "#this.modelNamespace#", 'home.index' )#"
			);

		}else{
			// Anythine value passed for action other than "add" will result in removing the menu

			menuService.removeSubMenu( 
				topMenu	= this.OISADMIN_TOPMENU, 
				name	= this.OISADMIN_SUBMENU
			);

			if( topMenuMap.keyExists( this.OISADMIN_TOPMENU ) ){
				if( !topMenuMap[ this.OISADMIN_TOPMENU ].subMenu.len() ){
					menuService.removeTopMenu( topMenu	= this.OISADMIN_TOPMENU );
				}
			}
			
		}

	} // manageAdminMenu()

}