<cfoutput>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>
                <i class="fa fa-code fa-lg" aria-hidden="true"></i> OIS Theme Settings Extender
                <a href="#prc.CBHelper.buildModuleLink("oisThemeSettings","home.index")#" class="btn btn-info pull-right" style="color: white;">
                    <i class="fas fa-cog"></i> Settings
                </a>
            </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12" id="oisThemeReadmeWrapper">
                    #prc.readme_html#
                </div>
            </div>
        </div>
    </div>
</cfoutput>
<!--- hide first h1 since heading is in the Panel with button --->
<script>
    document.addEventListener( "DOMContentLoaded", () => {
        $( "#oisThemeReadmeWrapper h1:first" ).hide();
        // Fix Anchor links because of issue with having base tag in header 
        // limit to only links inside the oisThemeReadmeWrapper div so we don't break the cbadmin menu
        $("#oisThemeReadmeWrapper a[href^='\#']").each(function() {
            this.href = location.href.split("#")[0] + '#' + this.href.substr(this.href.indexOf('#')+1);
        });
    });
</script>