<cfoutput>
<form id="cbContentBuilder-settings-form" method="post" action="#prc.saveURL#" role="form">
    <input type="hidden" name="_returnTo" value="#cb.linkSelf()#">

    <!--- Messageboxes --->
    #cbMessageBox().renderit()#

    <div class="panel panel-default">

        <div class="panel-heading">
            <h3>
                <i class="fa fa-code fa-lg" aria-hidden="true"></i> OIS Theme Settings Extender Configuration
                <a href="#prc.CBHelper.buildModuleLink("oisThemeSettings","home.about")#" class="btn btn-info pull-right" style="color: white;">
                    <i class="fas fa-question"></i> Help
                </a>
            </h3>
        </div>
    
        <div class="panel-body">

            <!--- INTRO SETTINGS --->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="badge pull-right" ><i class="far fa-clipboard"></i> Click Names Below to Copy</span>
                        <i class="fas fa-cogs"></i> #prc.oCurrentSite.getActiveTheme()# Theme Settings
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <cfscript>
                                buttonDefaults = event.getValue('buttonDefaults');
                            </cfscript>
                            <cfset groups = arrayNew(1) >
                            <ul class="list-group" style="max-height: 250px; overflow: auto; border: 1px solid ##ddd;">
                                <cfloop index="currentIndex" item="currentItem" array="#prc.themeService.getThemeRecord( prc.oCurrentSite.getActiveTheme() ).settings#"> 
                                    <cfif currentItem.keyExists("group")>
                                        <cfif !groups.findNoCase( currentItem.group ) >
                                            <li class="list-group-item btn-success">
                                                <h4 style=" margin: 0;">#currentItem.group#</h4>
                                                <cfif currentItem.keyExists("groupintro")>
                                                    <p>#currentItem.groupintro#</p>
                                                </cfif>
                                            </li>
                                            <cfset groups.append( currentItem.group ) >
                                        </cfif>
                                    </cfif>
                                    <cfscript>
                                        existingSetting = {
                                            "exists"    : false,
                                            "toolip"    : "",
                                            "icon"      : ''
                                        };
                                        if( currentItem.keyExists("oisImageSelect") ){
                                            existingSetting.exists = true;
                                            existingSetting.tooltop = "Image Select defined in theme settings";
                                            existingSetting.icon = buttonDefaults.images.icon;
                                        }else if( currentItem.keyExists("oisIconSelect") ){
                                            existingSetting.exists = true;
                                            existingSetting.tooltop = "Icon Select defined in theme settings";
                                            existingSetting.icon = buttonDefaults.icons.icon;
                                        }else if( currentItem.keyExists("oisLinkSelect") ){
                                            existingSetting.exists = true;
                                            existingSetting.tooltop = "Link Select defined in theme settings";
                                            existingSetting.icon = buttonDefaults.links.icon;

                                        }else if( currentItem.keyExists("oisJSONSelect") ){
                                            existingSetting.exists = true;
                                            existingSetting.tooltop = "JSON Editor defined in theme settings";
                                            existingSetting.icon = buttonDefaults.json.icon;
                                        }
                                    </cfscript><!---  <cfif existingSetting.exists >background-color: MediumSeaGreen;</cfif> --->
                                    <li class="list-group-item<cfif existingSetting.exists > btn-info</cfif>" style="padding-left: 35px;"
                                        <cfif existingSetting.exists >
                                            data-toggle="tooltip" data-placement="top" title="#existingSetting.tooltop#"
                                        </cfif> >
                                        <span   class="badge oisThemeSettingNameBadge" 
                                                style="cursor: copy; font-size: 1.1em;" id="__#currentItem.name#"
                                                onclick="copySettingKey('#currentItem.name#');" >
                                                <i class="far fa-clipboard"></i> #currentItem.name#
                                            </span>
                                        #currentItem.label# <cfif existingSetting.exists >#existingSetting.icon#</cfif>
                                    </li>
                                    <cfset existingSetting = javaCast( "null", "" ) >
                                </cfloop>
                            </ul>
                        </div>
                    </div>
                </div><!--- / panel-body --->
            </div>
            <!--- / INTRO SETTINGS --->

            <!--- GENERAL SETTINGS --->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fas fa-cogs"></i> General Settings</h3>
                </div>
                <div class="panel-body">
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                #html.label(
                                    class   = "control-label",
                                    field   = "enableThemeSettings",
                                    content = "Enable OIS Theme Settings Extender:"
                                )#
                                <div class="controls">
                                    #html.checkbox(
                                        name    = "enableThemeSettings_toggle",
                                        data	= { toggle: 'toggle', match: 'enableThemeSettings' },
                                        checked = event.getValue('enableThemeSettings')
                                    )#
                                    #html.hiddenField(
                                        name	= "enableThemeSettings",
                                        value 	= event.getValue('enableThemeSettings')
                                    )#
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!--- / panel-body --->
            </div>
            <!--- / GENERAL SETTINGS --->

            <!--- IMAGE SELECTOR SETTINGS --->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fas fa-images"></i> Image Selector Settings</h3>
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
                            #html.textArea(
                                name    		= "themeKeysImage",
                                value    		= event.getValue('themeKeysImage'),
                                label   		= "Image Selector Theme Settings Keys (comma seperated list):",
                                size    		= "50",
                                class   		= "form-control",
                                wrapper 		= "div class=controls",
                                labelClass 		= "control-label",
                                groupWrapper 	= "div class=form-group"
                            )#
                        </div>
                    </div>

                </div><!--- / panel-body --->
            </div>
            <!--- / IMAGE SELECTOR SETTINGS --->

            <!--- LINK SELECTOR SETTINGS --->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fas fa-sitemap"></i> Link Selector Settings</h3>
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
                            #html.textArea(
                                name    		= "themeKeysLinks",
                                value    		= event.getValue('themeKeysLinks'),
                                label   		= "Link Selector Theme Settings Keys (comma seperated list):",
                                size    		= "50",
                                class   		= "form-control",
                                wrapper 		= "div class=controls",
                                labelClass 		= "control-label",
                                groupWrapper 	= "div class=form-group"
                            )#
                        </div>
                    </div>

                </div><!--- / panel-body --->
            </div>
            <!--- / LINK SELECTOR SETTINGS --->

            <!--- JSON SELECTOR SETTINGS --->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fab fa-js"></i> JSON Editor Settings</h3>
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
                            #html.textArea(
                                name    		= "themeKeysJSON",
                                value    		= event.getValue('themeKeysJSON'),
                                label   		= "Link Selector Theme Settings Keys (comma seperated list):",
                                size    		= "50",
                                class   		= "form-control",
                                wrapper 		= "div class=controls",
                                labelClass 		= "control-label",
                                groupWrapper 	= "div class=form-group"
                            )#
                        </div>
                    </div>

                </div><!--- / panel-body --->
            </div>
            <!--- / JSON SELECTOR SETTINGS --->

            <!--- ICON SELECTOR SETTINGS --->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fas fa-icons"></i> Font-Awesome Icon Selector Settings</h3>
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
                            #html.textArea(
                                name    		= "themeKeysIcons",
                                value    		= event.getValue('themeKeysIcons'),
                                label   		= "Font Awesome Selector Theme Settings Keys (comma seperated list):",
                                size    		= "50",
                                class   		= "form-control",
                                wrapper 		= "div class=controls",
                                labelClass 		= "control-label",
                                groupWrapper 	= "div class=form-group"
                            )#
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            #html.select(
                                name    		= "faToLoad",
                                selectedValue   = event.getValue('faToLoad'),
                                options         = "#structKeyList( prc.faVersions )#",
                                label   		= "Font Awesome Version:",
                                class   		= "form-control",
                                wrapper 		= "div class=controls",
                                labelClass 		= "control-label",
                                groupWrapper 	= "div class=form-group"
                            )#
                            <p class="help-block">
                                <strong>Font-Awesome Versions Supported:</strong>
                                    <div class="row">
                                        <div class="col-sm-2">Version</div>
                                        <div class="col-sm-5">CDN URL Used</div>
                                        <div class="col-sm-5">Font-Awesome Version Page</div>
                                    </div>
                                <cfloop index="currentIndex" item="currentItem" array="#listToArray( structKeyList( prc.faVersions  ) )#"> 
                                    <div class="row">
                                        <div class="col-sm-2">#currentItem#</div>
                                        <div class="col-sm-5"><a href="#prc.faVersions[ currentItem ].url#" target="_blank">#prc.faVersions[ currentItem ].url#</a></div>
                                        <div class="col-sm-5"><a href="#prc.faVersions[ currentItem ].browse#" target="_blank">#prc.faVersions[ currentItem ].browse#</a></div>
                                    </div>
                                </cfloop>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                #html.label(
                                    class   = "control-label",
                                    field   = "faIncludeIconTag",
                                    content = "Include Icon HTML Tag:"
                                )#
                                <div class="controls">
                                    #html.checkbox(
                                        name    = "faIncludeIconTag_toggle",
                                        data	= { toggle: 'toggle', match: 'faIncludeIconTag' },
                                        checked = event.getValue('faIncludeIconTag')
                                    )#
                                    #html.hiddenField(
                                        name	= "faIncludeIconTag",
                                        value 	= event.getValue('faIncludeIconTag')
                                    )#
                                    <p class="help-block">When enabled the entire &lt;i class="fas fa-icons"&gt;&lt;/i&gt; will be returned. If disabled it will return just the CSS classes. Example: fas fa-icons</p>
                                </div>
                            </div>
                        </div>
                    </div>


                </div><!--- / panel-body --->
            </div>
            <!--- / ICON SELECTOR SETTINGS --->
            
            <div class="row" style="margin-top: 20px;">            
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success btn-send">Save Settings</button>
                </div>
            </div>
        
        </div>
    </div>
</form>
<script>
    function copySettingKey( inputName ){
 	    var tempInput = $("<input>");
        $( "body" ).append( tempInput );
        tempInput.val( inputName ).select();
        document.execCommand( "copy" );
        tempInput.remove();
        adminNotifier( "success", `${inputName} copied to clipboard`, "6000" );
        return false;
    }
</script>
</cfoutput>