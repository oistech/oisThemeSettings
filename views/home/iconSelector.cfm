<cfscript>
    jsonPath = prc.cbhelper.getmoduleService().getmoduleRegistry().oisThemeSettings.PHYSICALPATH & "/oisThemeSettings/includes/fadata/" & event.getValue("faToLoad") & ".json";
    allIcons = deserializeJSON( fileRead( jsonPath ) );
    iconStyles = listToArray( allIcons.prefix.keyList() );
    faVersions = event.getValue("faVersions")
    faThemes = event.getValue("faThemes");
</cfscript>
<cfoutput>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OIS Theme Extender Icon Selector</title>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/4.5.3/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!--- <script type="text/javascript" src="#prc.cbhelper.getmoduleService().getmoduleRegistry().oisThemeSettings.LOCATIONPATH#/oisThemeSettings/includes/css.min.js"></script> --->
        <link defer href='#faVersions[ event.getValue("faToLoad") ].url#' rel='stylesheet' type='text/css'>
        <link href='#prc.cbhelper.getmoduleService().getmoduleRegistry().oisThemeSettings.LOCATIONPATH#/oisThemeSettings/includes/css/oisTheme.css' rel='stylesheet' type='text/css'>
        <!--- <script type="text/javascript" src="#prc.cbhelper.getmoduleService().getmoduleRegistry().oisThemeSettings.LOCATIONPATH#/oisThemeSettings/includes/js/oisTheme.js"></script> --->
    </head>
    <body>
        <div class="card">
            <div class="card-header">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" id="iconSearchInput" placeholder="Search" aria-label="Search" aria-describedby="iconSearchClearButton">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" id="iconSearchClearButton">Clear</button>
                    </div>
                </div>
                <ul class="nav nav-tabs card-header-tabs">
                    <cfloop index="currentIndex" item="currentItem" array="#iconStyles#">
                        <cfif allIcons[ currentItem ].len()>
                            <li class="nav-item">
                                <a class="nav-link <cfif currentIndex EQ 1>active</cfif>" id="#currentItem#-tab" data-toggle="tab" href="###currentItem#-content" role="tab" aria-controls="#currentItem#-content" aria-selected="true">
                                    #faThemes[ currentItem ].tabTitle#
                                </a>
                            </li>
                        </cfif>
                    </cfloop>
                </ul>
            </div>
            <div class="card-body tab-content"  id="iconTabContent">
                <cfloop index="currentIndex" item="currentItem" array="#iconStyles#">
                    <cfif allIcons[ currentItem ].len()>
                        <div class="tab-pane fade <cfif currentIndex EQ 1>show active</cfif> iconBoxWrapper" id="#currentItem#-content" role="tabpanel" aria-labelledby="#currentItem#-tab">
                            <div class="iconBoxElements">
                                <cfloop index="iconIndex" item="iconItem" array="#allIcons[ currentItem ]#">
                                    <button class='iconBox' type="button" onclick="iconSelect( '#iconItem.class#' )" data-icon="#lcase(iconItem.id)#">
                                        <div class="iconTitle">#iconItem.id#</div>
                                        <div class="iconPreview"><i class="#iconItem.class# fa-2x"></i></div>
                                    </button>
                                </cfloop>
                            </div>
                        </div>
                    </cfif>
                </cfloop>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/4.5.3/js/bootstrap.bundle.js"></script>
        <script type="text/javascript">
            function iconSelect( iconClass ){
                console.log( iconClass );
                console.log( "#url.setting#" );

                parent.window.oisThemeSettings.selectIconCallback( "#url.setting#", iconClass );
            }
            function searchIcons( searchTerm ){
                if( searchTerm.length ){
                    $(`button.iconBox[data-icon*="${searchTerm.toLowerCase()}"]`).css("display", "inline-block");
                    $(`button.iconBox:not([data-icon*="${searchTerm.toLowerCase()}"])`).css("display", "none");
                }else{
                    $( "button.iconBox" ).css("display","inline-block");
                }
            }
            document.addEventListener( "DOMContentLoaded", () => {
                $("##iconSearchInput").keyup(function(){
                    searchIcons( $("##iconSearchInput").val() );
                });
                $("##iconSearchClearButton").click(function(){
                    $("##iconSearchInput").val("");
                    $( "button.iconBox" ).css("display","inline-block");
                });
            });
        </script>
    </body>
</html>
</cfoutput>